aws ec2 run-instances --instance-type m5.large \
--image-id ami-12345 \
--key-name michal-keys \
--subnet-id subnet-12345 \
--count 1 \
--region eu-central-1 \
--security-group-ids sg-12345 \
--user-data file://script.sh \
--block-device-mappings file://mapping.json