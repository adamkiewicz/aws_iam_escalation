aws iam list-roles
aws iam list-users
aws iam get-role --role-name XYZ
aws iam list-attached-role-policies --role-name XYZ    # for attached policies
aws iam list-role-policies --role-name XYZ     #for inline policies
aws iam get-role-policy --role-name XYZ --policy-name ABC   # works only with inline policies
aws iam get-policy --policy-arn arn:aws:iam::112233:policy/ABC # works only with attached policies and gives only version
aws iam get-policy-version --policy-arn arn:aws:iam::112233:policy/ABC --version-id v1 # works with attached policies and particular version

aws ec2 describe-vpcs
aws ec2 describe-subnets


aws ec2 describe-instances \
    --filter Name=tag-key,Values=Name \
    --region eu-central-1 \
    --query 'Reservations[*].Instances[*].{Instance:InstanceId,AZ:Placement.AvailabilityZone,Subnet:SubnetId,PrivateIp:PrivateIpAddress,PublicIp:PublicIpAddress,State:State.Name,Name:Tags[?Key==`Name`]|[0].Value}' \
    --output table


aws ec2 describe-instances \
    --filter Name=tag-key,Values=Name \
    --region eu-central-1 \
    --query 'Reservations[*].Instances[*].{Instance:InstanceId,Role:IamInstanceProfile.Arn,Name:Tags[?Key==`Name`]|[0].Value}' \
    --output table



#####PACU#####

python3 pacu.py

set-keys
run iam__enum_permissions
run iam__bruteforce_permissions