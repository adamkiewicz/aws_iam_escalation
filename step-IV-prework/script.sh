aws lambda create-function \
--function-name very-important-function \
--runtime python3.7 \
--role arn:aws:iam::1111:role/service-role/abcd  \
--handler lambda_function.lambda_handler \
--zip-file fileb://lambda_function.zip \
--environment Variables="{SITE=https://www.amazon.com/,EXPECTED=Amazon.com}" \
--region eu-central-1